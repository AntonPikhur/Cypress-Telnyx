Description:
Here you can find 10 functional test cases of Telnyx website (https://telnyx.com/). They created using Cypress e2e.
Test cases ./
Tests ./cypress/e2e/specs/
Page objects ./cypress/e2e/pages/

Need to instal befor use:
1. You need to visit https://docs.cypress.io/guides/getting-started/installing-cypress to instal framework.
2. Use " npm i -D cypress-if " to instal cypress-if plugin
3. Use " npm install -D cypress-xpath " to instla cypress-XPath plugin.

Commands:
To launch Cypress: npx cypress open